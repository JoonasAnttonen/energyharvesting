// ----------------------------------------
// Includes
// ----------------------------------------

#include "SCIF/ex_include_tirtos.h"
#include "SCIF/scif.h"
#include "string.h"
#include "stdio.h"
#include <ti/mw/display/Display.h>
#include <ti/drivers/adc/ADCCC26XX.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <driverlib/sys_ctrl.h>
#include <driverlib/aux_adc.h>
#include <driverlib/aon_batmon.h>

// ----------------------------------------
// Defines
// ----------------------------------------

typedef Task_Struct RTOSTask;
typedef Semaphore_Struct RTOSSemaphore;
typedef SCIF_RESULT_T ADCResult;
typedef xdc_runtime_Types_FreqHz CPUFrequency;
typedef _Bool bool_t;

// ----------------------------------------
// Globals
// ----------------------------------------

// RTOS task objects.
RTOSTask energyTaskHandle;
uint8_t energyTaskStack[1024];

PIN_State ledState;
PIN_Handle ledHandle;
PIN_Config LedPinTable[] = {
    Board_LED1    | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED0    | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

bool_t wakingUp = false;
PIN_Config WakeUpPinTable[] = {
    Board_BUTTON0 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
    PIN_TERMINATE
};

// ADC objects.
int32_t adcGainAdjust;
int32_t adcOffsetAdjust;
const uint16_t adcTaskId = 1 << SCIF_ADC_TASK_ID;
RTOSSemaphore adcSemaphore;

// Output driver objects.
#if !defined(BOARD_DISPLAY_EXCLUDE_UART)
UART_Handle uartHandle;
UART_Params uartParams;
#else
Display_Handle lcdHandle;
Display_Params lcdParams;
#endif

// ----------------------------------------
// outputOpen() - Initializes the output driver.
// ----------------------------------------
bool_t outputOpen()
{
#if !defined(BOARD_DISPLAY_EXCLUDE_UART)
	// Initialize UART parameters
	UART_Params_init(&uartParams);
	uartParams.baudRate = 115200;
	uartParams.writeDataMode = UART_DATA_TEXT;
	uartParams.dataLength = UART_LEN_8;
	uartParams.stopBits = UART_STOP_ONE;
	uartHandle = UART_open(Board_UART, &uartParams);
	return (uartHandle != 0);
#else
    Display_Params_init(&lcdParams);
    lcdParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    lcdHandle = Display_open(Display_Type_LCD, &lcdParams);
    return (lcdHandle != 0);
}
#endif

// ----------------------------------------
// outputClose() - Closes the output driver.
// ----------------------------------------
void outputClose()
{
#if !defined(BOARD_DISPLAY_EXCLUDE_UART)
	UART_close(uartHandle);
#else
	Display_clear(lcdHandle);
	Display_close(lcdHandle);
#endif
}

// ----------------------------------------
// adcReadyCallback() - Gets called when the ADC task has completed its run.
// ----------------------------------------
void adcReadyCallback()
{
	// Wake up the OS.
	Semaphore_post(Semaphore_handle(&adcSemaphore));
}

// ----------------------------------------
// adcOpen() - Initializes the ADC.
// ----------------------------------------
void adcOpen()
{
	// Get the ADC adjustment values.
	adcGainAdjust = AUXADCGetAdjustmentGain(AUXADC_REF_FIXED);
	adcOffsetAdjust = AUXADCGetAdjustmentOffset(AUXADC_REF_FIXED);

	// Initialize the ADC task.
	scifOsalInit();
	scifOsalRegisterCtrlReadyCallback(adcReadyCallback);
	scifInit(&scifDriverSetup);
}

// ----------------------------------------
// adcRead() - Resets the ADC task and executes it, return value indicates whether or not the operation succeeded.
// ----------------------------------------
bool_t adcRead()
{
	// Reset.
	scifResetTaskStructs(adcTaskId, SCIF_STRUCT_CFG | SCIF_STRUCT_INPUT | SCIF_STRUCT_OUTPUT);
	// Execute.
	ADCResult executeResult = scifExecuteTasksOnceNbl(adcTaskId);

	if ( executeResult != SCIF_SUCCESS )
	{
		return false;
	}

	// Wait for the control READY event from the ADC.
	Semaphore_pend(Semaphore_handle(&adcSemaphore), BIOS_WAIT_FOREVER);
	return true;
}

// ----------------------------------------
// adcGetChannelValue() - Get the latest ADC value in millivolts.
// ----------------------------------------
int32_t adcGetChannelValue(uint8_t channel)
{
	// Retrieve the raw value and adjust it.
	int32_t adcValue = scifTaskData.adc.output.Values[channel];
	adcValue = AUXADCAdjustValueForGainAndOffset(adcValue, adcGainAdjust, adcOffsetAdjust);
	adcValue = AUXADCValueToMicrovolts(4300000, adcValue);
	adcValue = adcValue / 1000;
	return adcValue;
}

// ----------------------------------------
// sleepFor(n) - Blocks the current task for n seconds.
// ----------------------------------------
void sleepFor(int n)
{
	Task_sleep(n * (1000000 / Clock_tickPeriod));
}

void hangIfFalse(int value)
{
	while (!value)
	{
		PIN_setOutputValue(ledHandle, Board_LED1, !PIN_getOutputValue(Board_LED1));
		sleepFor(1);
	}
}

void cpuShutdown()
{
    // Configure wake up and shutdown.
    PINCC26XX_setWakeup(WakeUpPinTable);
    Power_shutdown(NULL, 0);
}

// ----------------------------------------
// energyTask(a0, a1) - The task function that handles everything.
// ----------------------------------------
void energyTask(UArg a0, UArg a1)
{
    hangIfFalse(0);

	uint8_t i;

	// Initialize the ADC and data output.
	adcOpen();
	outputOpen();

	for(i = 0; i < 5; i++)
	{
		// Read the ADC.
		bool_t adcResult = adcRead();

		// Should not fail.
		hangIfFalse(adcResult);

		uint32_t batteryLevel = AONBatMonBatteryVoltageGet();
		batteryLevel = (batteryLevel * 125) >> 5;

		Display_print1(lcdHandle, 3, 0, "VDD: %4d mV", batteryLevel );
		Display_print1(lcdHandle, 5, 0, "DP0: %4d mV", adcGetChannelValue(0) );
		Display_print1(lcdHandle, 7, 0, "DP1: %4d mV", adcGetChannelValue(1) );

		// Sleep for a while.
		sleepFor(2);
	}

	outputClose();
	cpuShutdown();

    // If we get here, shutdown failed.
    hangIfFalse(0);
}

// ----------------------------------------
// main()
// ----------------------------------------
int main()
{
	Task_Params taskParams;
	Semaphore_Params semaphoreParams;

	// Initialize the PIN driver.
	PIN_init(BoardGpioInitTable);

	ledHandle = PIN_open(&ledState, LedPinTable);
	wakingUp = (SysCtrlResetSourceGet() == RSTSRC_WAKEUP_FROM_SHUTDOWN);

	// Create the OS task.
	Task_Params_init(&taskParams);
	taskParams.stack = energyTaskStack;
	taskParams.stackSize = sizeof(energyTaskStack);
	taskParams.priority = 3;
	Task_construct(&energyTaskHandle, energyTask, &taskParams, NULL);

	// Create the semaphore used to wait ADC events.
	Semaphore_Params_init(&semaphoreParams);
	semaphoreParams.mode = Semaphore_Mode_BINARY;
	Semaphore_construct(&adcSemaphore, 0, &semaphoreParams);

	// Start TI-RTOS
	BIOS_start();
	return 0;
}
